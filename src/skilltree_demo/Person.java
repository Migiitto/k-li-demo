/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skilltree_demo;



import java.io.File;
import java.io.InputStream;
import javafx.scene.image.Image;


/**
 *
 * @author Kimmo
 */
class Person {
   
        String nimi;
        String email;
        String osoite;
        String työtehtävä;
        String erityisosaaminen;
        String syntymävuosi;
        String puhelinnumero;
    
    
}

class Kimmo extends Person{
    public Kimmo(){
        nimi = "Kimmo Kiiskinen";
        email = "kimmo.kiiskinen@protonmail.ch";
        osoite = "Kurjenmutkantie 3AB";
        työtehtävä = "Asiantuntija";
        erityisosaaminen = "Oluen juonti";
        syntymävuosi = "1967";
        puhelinnumero = "050 1231234";
        
    }
}

class Kari extends Person {
    File file = new File("Karikuva.jpeg");
    Image image = new Image(file.toURI().toString());
    
    public Kari(){
        nimi = "Kari Karilla";
        email = "Kari.simo@Leena.org";
        osoite = "Skinnarila";
        työtehtävä = "Säheltäjä";
        erityisosaaminen = "Meikkaus";
        syntymävuosi = "1887";
        puhelinnumero = "050 0000001";
        
        

    }

}

