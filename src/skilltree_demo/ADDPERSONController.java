/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skilltree_demo;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Kimmo
 */
public class ADDPERSONController implements Initializable {
    Person person;
    datacenter data;
    
    ArrayList list = new ArrayList<Person>();
    @FXML
    private TextField nameField;
    @FXML
    private TextField emailField;
    @FXML
    private TextField addressField;
    @FXML
    private TextField jobField;
    @FXML
    private Button addPersonButton;
    @FXML
    private TextField yearField;
    @FXML
    private TextField numberField;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        data = datacenter.getInstance();
        
    }    

    @FXML
    private void addPersonToList(ActionEvent event) {
        person = new Person();
        person.nimi = nameField.getText();
        person.email = emailField.getText();
        person.osoite = addressField.getText();
        person.työtehtävä = jobField.getText();
        person.puhelinnumero = numberField.getText();
        person.syntymävuosi = yearField.getText();
        data.getList().add(person);
        
        
        
        
    }
    
}
