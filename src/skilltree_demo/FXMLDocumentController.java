/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skilltree_demo;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author Kimmo
 */
public class FXMLDocumentController implements Initializable {
    datacenter data;
    Person aku;
    
    
    private String name;
    
    
    @FXML
    private TextField SearchArea;
    @FXML
    private ListView<String> listArea;
    @FXML
    private Button CVButton;
    @FXML
    private Button LinkedInButton;
    @FXML
    private Button RepoButton;
    @FXML
    private Button InfoButton;
    @FXML
    private ImageView PictureView;
    @FXML
    private Button findButton;
    @FXML
    private Label nameId;
    @FXML
    private Label PuhId;
    @FXML
    private Label AdressId;
    @FXML
    private Label JobId;
    @FXML
    private Label eMailId;
    @FXML
    private Label yearId;
    @FXML
    private AnchorPane View1;
    @FXML
    private Button AddButton;
    @FXML
    private ComboBox<String> eduBox;
    @FXML
    private ComboBox<String> skillBox;
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        data = datacenter.getInstance();
        
        
        makeStuffForBox();
        
        
        
        
        
        
        
    }    

    @FXML
    private void showMore(ActionEvent event) throws IOException {
        Parent first_view = FXMLLoader.load(getClass().getResource("view2.fxml"));
        Scene first_view_scene  = new Scene(first_view);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(first_view_scene);
        app_stage.show();
        
        
    }

    @FXML
    private void AddPerson(ActionEvent event) throws IOException {
        Stage creator = new Stage();
        Parent page = FXMLLoader.load(getClass().getResource("ADDPERSON.fxml"));
        Scene scene = new Scene(page);
        creator.setScene(scene);
        creator.showAndWait();
        listArea.getItems().clear();
        for(int i = 0;i < data.getList().size(); i++){
            listArea.getItems().addAll(data.getList().get(i).nimi);
        }
        
    }

    @FXML
    private void findPerson(ActionEvent event) {
        eduBox.getItems().clear();
        skillBox.getItems().clear();
        makeStuffForBox();
    }

    @FXML
    private void showEducation(ActionEvent event) {
    }

    @FXML
    private void showSkills(ActionEvent event) {
    }
    
    private void makeStuffForBox(){
        eduBox.getItems().add("Kandidaatti");
        eduBox.getItems().add("Maisteri");
        eduBox.getItems().add("AMK");
        eduBox.getItems().add("TradenomiHomo");
        eduBox.getItems().add("Kari");
        skillBox.getItems().add("Java");
        skillBox.getItems().add("Koodaus");
        skillBox.getItems().add("Markkinointi");
        skillBox.getItems().add("Olla perseestä");
        skillBox.getItems().add("Oluen juonti");
        skillBox.getItems().add("Drinkit");
        skillBox.getItems().add("TiesKARIt");
    }

    @FXML
    private void getInfoAboutPerson(MouseEvent event) {
      name = listArea.getSelectionModel().getSelectedItem();
      for(int i = 0;i < data.getList().size(); i++){
            if(name.equals(data.getList().get(i).nimi)){
                nameId.setText(data.getList().get(i).nimi);
                PuhId.setText(data.getList().get(i).puhelinnumero);
                AdressId.setText(data.getList().get(i).osoite);
                JobId.setText(data.getList().get(i).työtehtävä);
                eMailId.setText(data.getList().get(i).email);
                yearId.setText(data.getList().get(i).syntymävuosi);   
          }
      }
      
    }

    @FXML
    private void updateView(ActionEvent event) {
        listArea.getItems().clear();
        if(data.getList().size() == 0){
            aku = new Person();
            aku.nimi = "Kimmo Kiiskinen";
            aku.email = "kimmo.kiiskinen@protonmail.ch";
            aku.osoite = "Kurjenmutkankuja 3B";
            aku.työtehtävä = "Asiantuntija";
            aku.puhelinnumero = "050 0000001";
            aku.syntymävuosi = "9.6.1934";
            data.getList().add(aku);
        }
        for(int i = 0;i < data.getList().size(); i++){
        listArea.getItems().addAll(data.getList().get(i).nimi);
        }
    }

}
