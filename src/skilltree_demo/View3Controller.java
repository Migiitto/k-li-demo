/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skilltree_demo;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Kimmo
 */
public class View3Controller implements Initializable {
    @FXML
    private TextArea textarea1;
    @FXML
    private TableView<?> table1;
    @FXML
    private TableView<?> table2;
    @FXML
    private TableView<?> table3;
    @FXML
    private TableColumn<?, String> table1row1;
    @FXML
    private TableColumn<?, String> table1row2;
    @FXML
    private TableColumn<?, String> table1row3;
    @FXML
    private TableColumn<?, ?> table2row1;
    @FXML
    private TableColumn<?, ?> table2row2;
    @FXML
    private TableColumn<?, ?> table3row1;
    @FXML
    private TableColumn<?, ?> table3row2;
    @FXML
    private TableColumn<?, ?> table3row3;
    @FXML
    private ListView<String> listview1;
    @FXML
    private ListView<String> listview2;
    @FXML
    private ListView<String> listview3;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> items1 =FXCollections.observableArrayList (
    "Käyttöliittymäsuunnittelu            Kari H                                1/16-5/16",
    "Tietokannat ja projekti                Antti H                               1/16-5/16",
    "Verkkosivut firmalle X                 Keijo S                                2/14-1/16",
    "Määrittelyduuni firmalle Y          Antti H                               12/13-2/14");
        ObservableList<String> items2 =FXCollections.observableArrayList (
    "LUT                                                                   9/10-12/13",
    "Sonera                                                             8/10-9/10",
    "S-Market Sammonlahti                                   5/10-8/10",
    "Posti                                                                 5/9-8/9");
        ObservableList<String> items3 =FXCollections.observableArrayList (
    "Ei suoritettuja kortteja");
        listview1.setItems(items1);
        listview2.setItems(items2);
        listview3.setItems(items3);
    }    

    @FXML
    private void changeView1(ActionEvent event) throws IOException {
        Parent view_two = FXMLLoader.load(getClass().getResource("view2.fxml"));
        Scene view_two_scene  = new Scene(view_two);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(view_two_scene);
        app_stage.show();
    }

    @FXML
    private void changeView2(ActionEvent event) {
    }

    
    
    @FXML
    private void changeView3(ActionEvent event) throws IOException {
        Parent view_two = FXMLLoader.load(getClass().getResource("view4.fxml"));
        Scene view_two_scene  = new Scene(view_two);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(view_two_scene);
        app_stage.show();
    }

    @FXML
    private void backToStart(ActionEvent event) throws IOException {
        Parent view_two = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene view_two_scene  = new Scene(view_two);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(view_two_scene);
        app_stage.show();
    }
    
}
