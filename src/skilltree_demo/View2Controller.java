/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skilltree_demo;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Kimmo
 */
public class View2Controller implements Initializable {
    @FXML
    private ListView<String> textView2;
    @FXML
    private ListView<String> textView1;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
               ObservableList<String> items1 =FXCollections.observableArrayList (
    "Suomi                                                             Äidinkieli",
    "Englanti                                                          Hyvä",
    "Ruotsi                                                             Kohtalainen",
    "Saksa                                                              Kohtalainen");
        ObservableList<String> items2 =FXCollections.observableArrayList (
    "Ei merkittäviä saavutuksia");
        textView1.setItems(items1);
        textView2.setItems(items2);
    }    

    @FXML
    private void changeView1(ActionEvent event) {
    }

    @FXML
    private void changeView2(ActionEvent event) throws IOException {
        Parent view_two = FXMLLoader.load(getClass().getResource("view3.fxml"));
        Scene view_two_scene  = new Scene(view_two);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(view_two_scene);
        app_stage.show();
    }

    @FXML
    private void changeView3(ActionEvent event) throws IOException {
        Parent view_two = FXMLLoader.load(getClass().getResource("view4.fxml"));
        Scene view_two_scene  = new Scene(view_two);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(view_two_scene);
        app_stage.show();
    }

    @FXML
    private void backToStart(ActionEvent event) throws IOException {
        Parent view_two = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene view_two_scene  = new Scene(view_two);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(view_two_scene);
        app_stage.show();
    }
    
}
