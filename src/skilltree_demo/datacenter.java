/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skilltree_demo;

import java.util.ArrayList;

/**
 *
 * @author Kimmo
 */
public class datacenter {
    private ArrayList<Person> list;
    
    static private datacenter data = null;
    
        private datacenter(){
            list = new ArrayList<Person>();
        }
        
        static public datacenter getInstance(){
            if(data == null){
                data = new datacenter();
            }
            
            return data;
        }

    /**
     * @return the list
     */
    public ArrayList<Person> getList() {
        return list;
    }
    
}
